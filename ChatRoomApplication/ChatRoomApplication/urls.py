"""
Definition of urls for ChatRoomApplication.
"""

from datetime import datetime
from django.conf.urls import url, include
import django.contrib.auth.views

# Uncomment the next lines to enable the admin:
# from django.conf.urls import include
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = [
    # Examples:
    url('', include('app.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
]

handler404 = 'app.views.error_page'
handler500 = 'app.views.error_page'
handler403 = 'app.views.error_page'
handler400 = 'app.views.error_page'