"""
Definition of forms.
"""

from django import forms
from django.contrib.auth.forms import AuthenticationForm
from . import models
from django.utils.translation import ugettext_lazy as _

class BootstrapAuthenticationForm(AuthenticationForm):
    """Authentication form which uses boostrap CSS."""
    username = forms.CharField(max_length=254,
                               widget=forms.TextInput({
                                   'class': 'form-control',
                                   'placeholder': 'User name'}))
    password = forms.CharField(label=_("Password"),
                               widget=forms.PasswordInput({
                                   'class': 'form-control',
                                   'placeholder':'Password'}))

class CreateNameForm(forms.Form):
    name = forms.CharField(max_length=30,required=True,
                           widget=forms.TextInput(attrs={'class':'form-control text-center input-lg input-w60-center','placeholder':'Enter your name'}))

class CreateUserForm(forms.ModelForm):
    class Meta:
        model = models.User
        fields = ['name']
        widgets = {'name':forms.TextInput(attrs={'class':'form-control text-center input-lg input-w60-center','placeholder':'Enter your name'})}

class SmileyForm(forms.Form):
	smiley = forms.CharField(required=True,
                           widget=forms.Textarea(attrs={'class':'form-control input-lg','rows':'2','id':'selected-smiley-textarea'}),)