
//function addEmoji(index, categoryIndex, emojiChar) {
//    //var emoji = new Object();
//    //emoji.index = index;
//    //emoji.categoryIndex = categoryIndex;
//    //emoji.emojiChar = emojiChar;
//    //selectedEmojis.push(emoji);
//    $("#selected-smiley-textarea").val(function () {
//        return this.value + "1";
//    });
//}

$('.char').click(function () {
//    var htmlData = $("#selected-smiley-textarea").val() + $(this).html();
    selectedEmojis.push($(this).html());
    $("#selected-smiley-textarea").val(selectedEmojis.join(' '));
    $('#selected-smiley-textarea').removeAttr('disabled');
    $("#selected-smiley-textarea").focus();
    $('#selected-smiley-textarea').attr('disabled','disabled');
    if(selectedEmojis.length>0){
      $("#bottomStick").show();
     }
});

$('#clear-button').click(function () {
//    var htmlData = $("#selected-smiley-textarea").val().slice(0, -2);
if(selectedEmojis.length!=0){
    selectedEmojis.pop();
    $("#selected-smiley-textarea").val(selectedEmojis.join(' '));
     $("#selected-smiley-textarea").focus();
     if(selectedEmojis.length==0){
      $("#bottomStick").hide();
     }
}
});

function makeEnable(){

 $('#selected-smiley-textarea').removeAttr('disabled');

}

var selectedEmojis = [];
var myCanvas;
var isMobile;

$(document).ready(function(){

$('#selected-smiley-textarea').keypress(function(e) {
    e.preventDefault();
});

$('#selected-smiley-textarea').on("cut copy paste",function(e) {
      e.preventDefault();
});

    isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    // $(document).on("click", '.whatsapp', function () {
    //});

});





$('.social-media-img').click(function () {
    $('#myModal').show();
});

$('.close').click(function () {
    $('#myModal').hide();
});

$('#rank_export').click(function () {
    $("#rank_download").hide();
    $('#myModal').show();
    var rankDiv=$('#export_rank');
    rankDiv.show();
  html2canvas(rankDiv, {
         scale: 2,
         onrendered: function (canvas) {
                myCanvas = canvas;
                var imgageData = canvas.toDataURL("image/png");
            // Now browser starts downloading it instead of just showing it
            var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
            $("#rank_download").show();
            rankDiv.hide();
            $("#rank_download").attr("href", newData)
             }
         });
});

window.onclick = function(event) {
    var modal = document.getElementById("myModal");
  if (event.target == modal) {
    $('#myModal').hide();
  }
}

function goBack() {
  window.history.back();
}


// Tooltip

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

$('.btn-clipboard').tooltip({
    trigger: 'click',
    placement: 'bottom'
});

function setTooltip(btn, message) {
    $(btn).tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
}

function hideTooltip(btn) {
    setTimeout(function () {
        $(btn).tooltip('hide');
    }, 1000);
}

// Clipboard

var clipboard = new ClipboardJS('.btn-clipboard');

clipboard.on('success', function (e) {
    setTooltip(e.trigger, 'Copied!');
    hideTooltip(e.trigger);
});

clipboard.on('error', function (e) {
    setTooltip(e.trigger, 'Failed!');
    hideTooltip(e.trigger);
});




