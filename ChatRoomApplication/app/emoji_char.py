
from typing import List, Dict


class Emoji:

	def __init__(self, data_blob: dict) -> None:
		self.name = data_blob.get("name")
		self.deci = data_blob.get("deci")
		self.hex = data_blob.get("hex")
		self.category = data_blob.get("Category")
		self.index = data_blob.get("index")
		self.categoryIndex = data_blob.get("CategoryIndex")
	
	@property
	def char(self) -> str:
		return chr(int(self.hex.lstrip("U+").zfill(8), 16))


class EmojiUser:
    def __init__(self,posted_by,posted_date,emoji_list,most_prefered_emoji) -> None:
        self.posted_by = posted_by
        self.posted_date = posted_date
        self.emoji_list = emoji_list
        self.most_prefered_emoji = most_prefered_emoji



