"""
Package for the application.
"""
from typing import List, Dict

from os import path
from .emoji_char import Emoji
import json

# Read json data on module load to be cached
with open(path.join(path.dirname(__file__), "data/emoji.json"), "r") as full_data:
    # Load and parse emoji data from json into Emoji objects
    # pylint: disable=invalid-name
    emoji_data = [Emoji(data_blob) for data_blob in json.loads(full_data.read())]  # type: List[Emoji]
