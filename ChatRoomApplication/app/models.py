"""
Definition of models.
"""

from django.db import models
from datetime import datetime
import uuid 

# Create your models here.

class Name(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

class User(models.Model):
    name = models.CharField(max_length=30, null = False, blank = False)
    chat_room_id = models.UUIDField(default= uuid.uuid4, null = False, blank = False, unique=True)
    created_date = models.DateTimeField(auto_now_add=True, null = False, blank = False)
    last_interacted_date = models.DateTimeField(auto_now_add=True, null = False, blank = False)

    def __str__(self):
        return self.name

#models.ForeignKey(Class,on_delete=models.CASCADE)
#models.ManyToManyField(Class)

class Smiley(models.Model):
    posted_by = models.CharField(max_length=30, null = False, blank = False)
    posted_date = models.DateTimeField(auto_now_add=True, null = False, blank = False)
    chat_room_id = models.UUIDField(null = False, blank = False)
    created_for_user = models.ForeignKey(User,on_delete = models.CASCADE)
    emoji_decimals = models.TextField(null = False, blank = False)
    emoji_hexs = models.TextField(null = False, blank = False)
    