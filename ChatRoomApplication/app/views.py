"""
Definition of views.
"""
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.shortcuts import render, redirect, HttpResponse, render_to_response
from django.http import HttpRequest
from django.template import RequestContext
from django.utils import timezone
from datetime import datetime
from . conversion import *
from django.core.exceptions import ValidationError
from . forms import CreateUserForm, CreateNameForm, SmileyForm
from app import emoji_data
from . models import Smiley, User
from . emoji_char import EmojiUser
from django.conf import settings

extraContext = {'title':'Emoji','company_name':settings.NAME,'company_mail':settings.EMAIL,'company_insta_url':settings.INSTA_URL,'company_fb_url':settings.FB_URL}

def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Home Page',
            'year':datetime.now().year,
        }
    )

def contact(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contact.html',
        {
            'title':'Contact',
            'message':'Your contact page.',
            'year':datetime.now().year,
        }
    )

def about(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        {
            'title':'About',
            'message':'Your application description page.',
            'year':datetime.now().year,
        }
    )

def userNotFoundPage(request):
    return render(request,'app/user_not_found.html',extraContext)

def error_page(request,exception):
    return render(request,'app/user_not_found.html',extraContext)

def contact_us(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/contactus.html',
        extraContext
    )

def about_us(request):
    """Renders the about page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/about.html',
        extraContext
    )

def privacy_policy(request):
    """Renders the contact page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/privacypolicy.html',
        extraContext
    )

def name(request):
    if request.method == 'POST' :
        form=CreateNameForm(request.POST)
        if form.is_valid():
            return redirect('home')
    else:
        form = CreateNameForm()
    context = {'form':form}
    return render(request,'app/name.html',context)


def createUser(request):
    if('createdUser' in request.COOKIES):
        return redirect(userDashboardPage, uuid = request.COOKIES['createdUser'])
    if request.method == 'POST' : 
        form = CreateUserForm(request.POST)
        if form.is_valid():
            userDetails=form.save(commit = False)
            userDetails.save()
            response = redirect(sharePage,uuid = userDetails.chat_room_id)
            response.set_cookie('createdUser',userDetails.chat_room_id)
            response.set_cookie('createdUserId',userDetails.id)
            return response
    else :
        form = CreateUserForm()
    context = {'form':form}
    context.update(extraContext)
    return render(request,'app/createuser.html',context)

badgeNames = ['No friends','Casual friends','Good friends','Close friends','Exciting friends','Lifetime friends']
userBadgeCount = [0,5,10,20,30,40]
emojiBadgeCount = [0,20,40,60,90,120]
badges = [static('app/images/1.png'),static('app/images/2.png'),static('app/images/3.png'),static('app/images/4.png'),static('app/images/5.png'),static('app/images/6.png')]
badges2x = [static('app/images/1@2x.png'),static('app/images/2@2x.png'),static('app/images/3@2x.png'),static('app/images/4@2x.png'),static('app/images/5@2x.png'),static('app/images/6@2x.png')]

def registeredUsersCommonDetails(user,uuid):

    smileys_for_the_user = user.smiley_set.all().order_by('-posted_date')

    emojiUserList = []
    totalEmojiForTheUser=[]
    for record in smileys_for_the_user:
        emojiList=[]
        for hex in record.emoji_hexs.split(","):
            emoji = next(filter(lambda emoji: emoji.hex == hex ,emoji_data),None)
            #data_blob = {}
            #data_blob['hex'] = hex
            #emoji = Emoji(data_blob)
            emojiList.append(emoji)
        most_prefered_emoji = most_frequent_emoji(emojiList) 
        totalEmojiForTheUser.extend(emojiList)
        emojiUser = EmojiUser(record.posted_by,record.posted_date,emojiList,most_prefered_emoji)
        emojiUserList.append(emojiUser)
    most_prefered_emoji_for_the_user = None
    if len(totalEmojiForTheUser) != 0 :
        most_prefered_emoji_for_the_user = most_frequent_emoji(totalEmojiForTheUser)    

    usersBadges = getUserBadges(smileys_for_the_user.count(),len(totalEmojiForTheUser))

    context = {'uuid':uuid,'emojis':emojiUserList,"user":user,"perfered_emoji":most_prefered_emoji_for_the_user,
               'userBadges':zip(usersBadges,badgeNames),'userBadgesPrint':zip(usersBadges,badgeNames),'userBadgesPopUp':zip(usersBadges,badgeNames)}
    context.update(extraContext)
    return context

def getUserBadges(userCount,emojiCount):
    index = 0
    if userCount < 5:
        index = 0
    elif userCount >= 5 and userCount < 10:
        index = 1 
    elif userCount >= 10 and userCount < 20:
        index = 2
    elif userCount >= 20 and userCount < 30:
        index = 3 
    elif userCount >= 30 and userCount < 40:
        index = 4
    else :
        index = 5
    if emojiCount < 20 and index <=0:
        index = 0
    elif emojiCount >= 20 and emojiCount < 40 and index <= 1:
        index = 1 
    elif emojiCount >= 40 and emojiCount < 60 and index <= 2:
        index = 2
    elif emojiCount >= 60 and emojiCount < 90 and index <= 3:
        index = 3 
    elif emojiCount >= 90 and emojiCount < 120 and index <= 4:
        index = 4
    elif index <= 5:
        index = 5
    
    index=index + 1

    return badges2x[0:index]

def badgeScreen(request):
    context = {'badges':zip(badges2x,badgeNames,userBadgeCount,emojiBadgeCount)}
    context.update(extraContext)
    return render(request,'app/badges.html',context)

def homeScreen(request):
    return render(request,'app/home.html',extraContext)

def userDashboardPage(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :
        return  userNotFoundPage(request) , {}

    context = registeredUsersCommonDetails(user,uuid)
    isCreatedUser = 'createdUser' in request.COOKIES and request.COOKIES['createdUser'] == uuid
    context['isCreatedUser'] = isCreatedUser
    context['isDashboard'] = True
    
    return render(request,'app/userdashboard.html',context)


def sharedEmotionsInitialPage(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :
        return  userNotFoundPage(request) , {}

    if('createdUser' in request.COOKIES and request.COOKIES['createdUser'] == uuid):
        return redirect(userDashboardPage, uuid = uuid)

    context = registeredUsersCommonDetails(user,uuid)

    return render(request,'app/share_emotions_initial.html',context)

def postedEmotions(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :
        return  userNotFoundPage(request) , {}

    if('createdUser' in request.COOKIES and request.COOKIES['createdUser'] == uuid):
        return redirect(userDashboardPage, uuid = uuid)

    context = registeredUsersCommonDetails(user,uuid)
    httpResponse = render(request,'app/smiley_posted.html',context)
    httpResponse.delete_cookie('username')
    return httpResponse

def sharePage(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :         
        return  redirect(userNotFoundPage)
    url ='http://%s%s' % (request.get_host(), '/emotions/'+uuid)
    context = {'user':user,'uuid':uuid,'share_url':url}
    context.update(extraContext)
    return render(request,'app/share.html',context)



def postSmileyUser(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :         
        return  redirect(userNotFoundPage)

    if('createdUser' in request.COOKIES and request.COOKIES['createdUser'] == uuid):
        return redirect(userDashboardPage, uuid = uuid)

    if(request.method=='POST'):
        form = CreateNameForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data.get('name')
            response = redirect(postSmileyByUser, uuid = uuid)
            response.set_cookie('username',name)
            return response
    else:
        form = CreateNameForm()
    context = {'uuid':uuid,'form':form}
    context.update(extraContext)
    return render(request,'app/postsmileyuser.html',context)



def postSmileyByUser(request,uuid):
    try:
        user = User.objects.get(chat_room_id=uuid)
        user.last_interacted_date = timezone.now()
        user.save()
    except (User.DoesNotExist,ValidationError) as e :         
        return  redirect(userNotFoundPage)

    if('username' not in request.COOKIES):
        return redirect(postSmileyUser,uuid=uuid)
    if(request.method=='POST'):
        form = SmileyForm(request.POST)
        if form.is_valid():
            smiley = form.cleaned_data.get('smiley')
            hexs = [];
            decimals = [];
            for i in smiley.split():
                hexs.append(char_to_unified(i))
            for hex in hexs:
                emoji = next(filter(lambda emoji: emoji.hex == hex ,emoji_data),None)
                if(emoji!=None):
                    decimals.append(emoji.deci)
            hexStr = ','.join(map(str, hexs)) 
            decimalStr = ','.join(map(str, decimals))
            userName = request.COOKIES['username']
            posted_smileys = Smiley(posted_by=userName,chat_room_id=uuid,created_for_user=user,emoji_decimals=decimalStr,emoji_hexs=hexStr)
            posted_smileys.save()
            return redirect(postedEmotions,uuid=uuid)
    else:
        form = SmileyForm()
    categories = [None] * 9
    emojiList = [None] * 9
    for i in range(9):
        emojiList[i] = list(filter(lambda emoji: emoji.categoryIndex == i+1,emoji_data))
        categories[i] = emojiList[i][0].category

    context = {'emojiList':emojiList,'form':form,'categories':categories,'user':user}
    context.update(extraContext)
    return render(request,'app/postsmileybyuser.html',context)