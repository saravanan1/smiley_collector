import re
from django.core.exceptions import ValidationError

def emoji_validator(value):
    split_count = len(value.split())
    pattern = '\u00a9|\u00ae|[\u2000-\u3300]|\ud83c[\ud000-\udfff]|\ud83d[\ud000-\udfff]|\ud83e[\ud000-\udfff]'
    str= value.encode('unicode_escape')
    getStrings = re.findall(pattern,str)
    regex_count = len(re.findall(pattern,str))
    if split_count != regex_count :
        raise ValidationError("Invalid Characters found")
