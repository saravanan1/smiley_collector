from django.conf.urls import url

from . import views

urlpatterns = [
    url('create/',views.createUser,name="create",),
    url(r'^userdashboard/(?P<uuid>[0-9a-z-]+)',views.userDashboardPage,name="dashboard"),
    url(r'^share/(?P<uuid>[0-9a-z-]+)',views.sharePage,name="share_page"),
    url(r'^emotions/(?P<uuid>[0-9a-z-]+)',views.sharedEmotionsInitialPage,name="shared_intial_page"),
    url(r'^react/user/(?P<uuid>[0-9a-z-]+)',views.postSmileyUser,name="smiley_user"),
    url(r'^react/smiley/(?P<uuid>[0-9a-z-]+)',views.postSmileyByUser,name="react_smiley"),
    url(r'^completed/(?P<uuid>[0-9a-z-]+)',views.postedEmotions,name="smiley_posted_successfully"),
    url('badges/',views.badgeScreen,name="badges"),
    url(r'^contact/', views.contact_us, name='contact'),
    url(r'^about/', views.about_us, name='about'),  
    url(r'^privacypolicy/', views.privacy_policy, name='privacypolicy'),
    url('',views.homeScreen,name="home")
    ]